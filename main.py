"""
Copyright (C) 2013, 2014, 2015, 2016 Digital Freedom Foundation
Copyright (C) 2017, 2018, 2019, 2020, 2021, 2022 Digital Freedom Foundation & Accion Labs Pvt. Ltd.
  This file is part of GNUKhata:A modular,robust and Free Accounting System.

  GNUKhata is Free Software; you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation; either version 3 of
  the License, or (at your option) any later version.

  GNUKhata is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public
  License along with GNUKhata (COPYING); if not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA  02110-1301  USA59 Temple Place, Suite 330,


Contributors
============
Sai Karthik <kskarthik@disroot.org>

"""

# datetime
# from email import utils

# from feedgen.feed import FeedGenerator
# date = utils.format_datetime(datetime.datetime.now())

import requests
from os import getenv

services = {
    "gkcore-test-server": "https://api-dev.gnukhata.org/state",
    "openapi-spec-file": "https://api-dev.gnukhata.org/spec/main.yaml",
    "gkapp": "https://try.gnukhata.org",
    "website": "https://gnukhata.org",
}


def send_msg(txt):
    """Send message to telegram @gnukhata_bot via API call"""
    requests.post(
        f"https://api.telegram.org/bot{getenv('GKBOT_API_KEY')}/sendMessage?chat_id=@gnukhata_devel&text={txt}"
    )


for service in services:
    print(f"Checking {service} -> {services[service]}")
    try:
        r = requests.get(url=services[service], verify=False)
        if r.status_code != 200:
            send_msg(f"🆘 {service} is Down\n\n {r.text}")
    except Exception as e:
        print(e)
        send_msg(f"⚠ {service} is Interrupted\n\n {e}")

# """
# Generate XML Feeds
# """
# fg = FeedGenerator()
# fg.id("http://lernfunk.de/media/654321")
# fg.title("GNUKhata Service Status")
# fg.author({"name": "GNUKhata Team", "email": "support@gnukhata.in"})
# fg.link(href="http://gnukhata.in", rel="alternate")
# fg.logo("http://ex.com/logo.jpg")
# fg.subtitle("Updates on various services run by gnukhata team")
# fg.link(href="http://larskiesow.de/test.atom", rel="self")
# fg.language("en")


# for service in services:
#     print(f"Checking {service} -> {services[service]}")
#     try:
#         r = requests.get(url=services[service])
#         if r.status_code != 200:
#             fe = fg.add_entry()
#             fe.id(service)
#             fe.title(f"{service} is down | {r.status_code} Error")
#             fe.summary(r.text)
#             fe.link(href=services[service])
#             fe.pubDate(date)

#     except Exception as e:
#         print(e)
#         fe = fg.add_entry()
#         fe.id(service)
#         fe.title(f"{service} is interrupted")
#         fe.summary(str(e))
#         fe.pubDate(date)
#         fe.link(href=services[service])

# print("Generating Feeds")

# fg.atom_file("atom.xml")  # write the atom feed to a file
# fg.rss_file("rss.xml")  # Write the RSS feed to a file

# items = ""
# for service in services:
#     print(f"Checking {service} -> {services[service]}")
#     try:
#         r = requests.get(url=services[service])
#         if r.status_code != 200:
#             items += f"""
#             <item>
#                 <title>{service} is down on {date}</title>
#                 <link>{services[service]}</link>
#                 <description>
#                     {r.json()}
#                 </description>
#                 <pubDate>{date}</pubDate>
#                 <guid>{date}</guid>
#             </item>
#             """
#     except Exception as e:
#         items += f"""
#             <item>
#                 <title>{service} is down on {date}</title>
#                 <link>{services[service]}</link>
#                 <description>
#                     {e}
#                 </description>
#                 <pubDate>{date}</pubDate>
#                 <guid>{date}</guid>
#             </item>
#             """
#         # xmpp_send_msg(e)


# xml_head = f"""<?xml version="1.0" encoding="UTF-8" ?>
# <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom>
# <channel>
#   <title>GNUKhata Service Status</title>
#   <link>https://gnukhata.gitlab.io/state</link>
#   <description>Uptime Status of gnukhata related services</description>
#   <lastBuildDate>{date}</lastBuildDate>
#   <atom:link href="https://kskarthik.gitlab.io/state/rss.xml" rel="self" type="application/rss+xml"/>
#   {items}
# </channel>
# </rss>
# """
# with open("rss.xml", "w") as f:
#     f.write(xml_head)
